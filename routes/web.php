<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])->prefix('adminpanel')->group(function () {
	Route::get('/', function () {
	    return redirect('adminpanel/dashboard');
	});
	Route::get('/dashboard', [App\Http\Controllers\Be\DashboardController::class, 'index']);
	Route::post('/dashboard', [App\Http\Controllers\Be\DashboardController::class, 'store']);
	Route::get('/change-password', [App\Http\Controllers\Be\PasswordController::class, 'index']);
	Route::post('/change-password', [App\Http\Controllers\Be\PasswordController::class, 'store']);
	Route::resource('/service', App\Http\Controllers\Be\ServiceController::class);
	Route::resource('/testimoni', App\Http\Controllers\Be\TestimoniController::class);
	Route::resource('/pricing', App\Http\Controllers\Be\PricingController::class);
	Route::get('/email', [App\Http\Controllers\Be\EmailController::class, 'index']);
	Route::get('/orders', [App\Http\Controllers\Be\OrdersController::class, 'index']);
	Route::delete('/orders/{id}', [App\Http\Controllers\Be\OrdersController::class, 'destroy']);
});

Route::get('/', [App\Http\Controllers\Fe\HomeController::class,'index']);
Route::post('/', [App\Http\Controllers\Fe\HomeController::class,'store']);
Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
