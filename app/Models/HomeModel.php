<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomeModel extends Model
{
    use HasFactory;

    protected $table = "tb_table_home";

    protected $fillable = [
        'twitter',
        'instagram',
        'facebook',
        'email',
        'address',
        'phone',
        'title_1',
        'desc_1',
        'title_2',
        'desc_2a',
        'desc_2b',
        'about_us'
    ];}
