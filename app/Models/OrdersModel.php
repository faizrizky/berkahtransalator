<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrdersModel extends Model
{
    use HasFactory;

    protected $table = "tb_table_orders";

    protected $fillable = [
        'id_pricing',
        'name',
        'email',
        'phone',
        'subject',
        'file',
        'message',
        'sent_email',
    ];

    // relation

    public function pricing()
    {
        return $this->hasOne('App\Models\PricingModel', 'id', 'id_pricing');
    }
}
