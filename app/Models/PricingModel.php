<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PricingModel extends Model
{
    use HasFactory;

    protected $table = "tb_table_pricing";

    protected $fillable = [
        'title',
        'price',
        'content',
        'highlight',
        'show',
    ];}
