<?php

namespace App\Http\Controllers\Be;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PricingModel;

use DB;
use File;
use Input;

class PricingController extends Controller
{
    private $active = ['pricing'=>'active', 'title' => 'pricing'];

    function __construct(){
        $this->active['url'] = url('adminpanel/pricing');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active = $this->active;
        $data = PricingModel::orderBy('title')->get();
        return view('backend.pricing.index', compact('active', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [];
        $rule_msg = [];

        $data = $request->all();
        $id = isset($data['id']) ? $data['id']:0;
        $data['show'] = isset($data['show']) ? $data['show']:null;
        $data['highlight'] = isset($data['highlight']) ? $data['highlight']:null;
        $msg = "";
        try {
            $pricing = PricingModel::updateOrCreate(
                ['id' => $id],
            $data);
            $msg = "Operation success";
        } catch (\Exception $e) {
            $msg = $e->getMessage();
        }
        \Session::flash('msg', $msg);
        return redirect($this->active['url']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $active = $this->active;
        $data = PricingModel::findOrFail($id);
        return view('backend.pricing.form', compact('active', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
    }
}
