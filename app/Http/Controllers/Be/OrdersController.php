<?php

namespace App\Http\Controllers\Be;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\OrdersModel;

use DB;
use File;
use Input;

class OrdersController extends Controller
{
    private $active = ['orders'=>'active', 'title' => 'orders','upload_path' => 'app/public/home',
        'download_path' => 'app/public/home'];

    function __construct(){
        $this->active['url'] = url('adminpanel/orders');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $active = $this->active;
        $date = ($request->filter) ? $request->filter:Carbon::now()->format('d-m-Y');

        $start_date = Carbon::parse($date)->startOfDay()->format('Y-m-d H:i:s');
        $end_date = Carbon::parse($date)->EndOfDay()->format('Y-m-d H:i:s');
        $data = OrdersModel::where('created_at', '>=', $start_date)
                    ->where('created_at', '<=', $end_date)->orderBy('id', 'DESC')->get();
        return view('backend.orders.index', compact('active', 'data', 'date'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = OrdersModel::findOrFail($id);
        \Myhelper::delete_file($this->active['upload_path'],$data->file);
        $data->delete();
        \Session::flash('msg', "Operation success");
        return redirect($this->active['url']);
    }

}
