@extends('layouts.backend')
@section('title')
  {{ucfirst($active['title'])}}
@endsection
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ucfirst($active['title'])}}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <section class="col-lg-12">
          @if (Session::has('msg'))
          <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <center>
              <strong>{{ session('msg') }}</strong>
            </center>
          </div>
          @endif
          @if($errors->any())
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <center>
                  <strong>{{ implode('', $errors->all('<div>:message</div>')) }}</strong>
                </center>
              </div>
          @endif
        	<div class="box">
            <div class="box-header">
              <h3 class="box-title">Table Service</h3>
              
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table id="dt-1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Title</th>
                  <th>Price</th>
                  <th>Content</th>
                  <th>Show</th>
                  <th>Highlight</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($data) && $data)
                  @foreach($data as $idx => $row)
                    <tr>
                      <td>{{++$idx}}</td>
                      <td>{{$row->title}}</td>
                      <td>{{$row->price}}</td>
                      <td>{{$row->content}}</td>
                      <td>
                        <center>
                        @if($row->show)
                          <i class="fa fa-check text-info"></i>
                        @else
                          <i class="fa fa-times text-danger"></i>
                        @endif
                        </center>
                      </td>
                      <td>
                        <center>
                        @if($row->highlight)
                          <i class="fa fa-check text-info"></i>
                        @else
                          <i class="fa fa-times text-danger"></i>
                        @endif
                        </center>
                      </td>
                      <td>
                        <center>
                          <a href="{{url($active['url'])}}/{{$row->id}}/edit"><i class="fa fa-edit"></i></a>
                        </center>
                      </td>
                    </tr>
                  @endforeach
                @endif
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    <form action="" method="post" id="form-delete">
        @csrf
        @method('delete')
    </form>
@endsection
@push('appjs')
<script>
  $(function () {
    $('#dt-1').DataTable()
  });
  const deleteData = (id) => {
      swal({
        title: "Delete",
        text: "Are you sure to delete this data?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $(`#form-delete`).attr('action', `{{$active['url']}}/${id}`);
          $(`#form-delete`).submit();
        } 
      });
    }
</script>
@endpush