@extends('layouts.backend')
@section('title')
  {{ucfirst($active['title'])}}
@endsection
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ucfirst($active['title'])}}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <section class="col-lg-12">
        	<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Service Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="{{$active['url']}}" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="id" value="{{$data->id ?? 0}}">
              <div class="box-body">
                <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Title</label>

                  <div class="col-sm-10">
                    <input type="text" name="title" class="form-control" id="title" placeholder="title" value="{{$data->title ?? old('title')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="price" class="col-sm-2 control-label">Price</label>

                  <div class="col-sm-10">
                    <input type="text" name="price" class="form-control" id="price" placeholder="price" value="{{$data->price ?? old('price')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="show" class="col-sm-2 control-label">Show</label>

                  <div class="col-sm-10">
                    <input type="checkbox" name="show" class="checkbox" id="show" placeholder="show" value="1" {{(isset($data->show) && $data->show == 1) ? 'checked':''}}>
                  </div>
                </div>
                <div class="form-group">
                  <label for="highlight" class="col-sm-2 control-label">Highlight</label>

                  <div class="col-sm-10">
                    <input type="checkbox" name="highlight" class="checkbox" id="highlight" placeholder="highlight" value="1" {{(isset($data->highlight) && $data->highlight == 1) ? 'checked':''}}>
                  </div>
                </div>
                <div class="form-group">
                  <label for="highlight" class="col-sm-2 control-label">Content</label>

                  <div class="col-sm-10">
                    <textarea class="form-control" rows="7" placeholder="" name="content">{{$data->content}}</textarea>
                    <span class="text-danger">Separate with enter for each part of pricing table</span>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a type="button" href="{{$active['url']}}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Save</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
@endsection