@extends('layouts.backend')
@section('title')
  {{ucfirst($active['title'])}}
@endsection
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ucfirst($active['title'])}}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <section class="col-lg-12">
          @if (Session::has('msg'))
          <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <center>
              <strong>{{ session('msg') }}</strong>
            </center>
          </div>
          @endif
          @if($errors->any())
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <center>
                  <strong>{{ implode('', $errors->all('<div>:message</div>')) }}</strong>
                </center>
              </div>
          @endif
          <div class="form-group">
            <label>Filter:</label>

            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text" class="form-control pull-right" id="datepicker-filter" value="{{$date ?? ''}}">
            </div>
            <!-- /.input group -->
          </div>
        	<div class="box">
            <div class="box-header">
              <h3 class="box-title">Table Testimoni</h3>
              {{-- <a href="{{$active['url']}}/create" type="button" class="btn btn-primary btn-xs pull-right">
              	<i class="fa fa-plus"></i> New
              </a> --}}
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table id="dt-1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Subject</th>
                  <th>Package</th>
                  <th>Message</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($data) && $data)
                  @foreach($data as $idx => $row)
                    <tr>
                      <td>{{++$idx}}</td>
                      <td>{{$row->name}}</td>
                      <td>{{$row->email}}</td>
                      <td>{{$row->phone}}</td>
                      <td>{{$row->subject}}</td>
                      <td>{{$row->pricing->title}}</td>
                      <td>{{$row->message}}</td>
                      <td>
                        <center>
                          <a href="{{url('')}}/{{$active['download_path']}}/{{$row->file ?? ''}}" download="">
                            <i class="fa fa-download"></i>
                          </a>
                          &nbsp
                          <a href="javascript:;" onclick="deleteData({{$row->id}})"><i class="fa fa-trash"></i></a>
                        </center>
                      </td>
                    </tr>
                  @endforeach
                @endif
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    <form action="" method="post" id="form-delete">
        @csrf
        @method('delete')
    </form>
@endsection
@push('appjs')
<script>
  $(function () {
    $('#dt-1').DataTable()
  });
  const deleteData = (id) => {
      swal({
        title: "Delete",
        text: "Are you sure to delete this data?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $(`#form-delete`).attr('action', `{{$active['url']}}/${id}`);
          $(`#form-delete`).submit();
        } 
      });
    }
  $('#datepicker-filter').datepicker({
    autoclose: true,
    format : "dd-mm-yyyy",
  }).on('changeDate', function(e) {
        window.location.href = `{{$active['url']}}?filter=${$(this).val()}`;
    });
</script>
@endpush