@extends('layouts.backend')
@section('title')
  {{ucfirst($active['title'])}}
@endsection
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ucfirst($active['title'])}}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <section class="col-lg-12">
          @if (Session::has('msg'))
          <div class="alert alert-{{ session('alert') }} alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <center>
              <strong>{{ session('msg') }}</strong>
            </center>
          </div>
          @endif
          @if ($errors->any())
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
        	<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Dashboard Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="{{$active['url']}}">
              @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="password" class="col-sm-2 control-label">New Password</label>

                  <div class="col-sm-10">
                    <input type="password" name="password" class="form-control" id="password" placeholder="password" required="" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="password_confirmation" class="col-sm-2 control-label">Repeat Password</label>

                  <div class="col-sm-10">
                    <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="repeat password" required="" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="old_password" class="col-sm-2 control-label">Old Password</label>
                  <div class="col-sm-10">
                    <input type="password" name="old_password" class="form-control" id="old_password" placeholder="old password" required="" value="">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a type="button" href="{{$active['url']}}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Save</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
@endsection
@push('appjs')
<script>
  $(function () {
    const calculation = (is_auto) => {
      if (is_auto == 1) {
        $('#hide-score').hide();
        $('input[name=score]').val(0);
      }else{
        $('#hide-score').show();
      }
    }
    $('#calculation').change(function(e) {
      calculation($(this).val());
    });
    @if (isset($data->is_auto))
      calculation({{$data->is_auto}});
    @endif
  })
</script>
@endpush