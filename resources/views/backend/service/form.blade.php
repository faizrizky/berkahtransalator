@extends('layouts.backend')
@section('title')
  {{ucfirst($active['title'])}}
@endsection
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ucfirst($active['title'])}}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <section class="col-lg-12">
        	<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Service Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="{{$active['url']}}" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="id" value="{{$data->id ?? 0}}">
              <div class="box-body">
                <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Title</label>

                  <div class="col-sm-10">
                    <input type="text" name="title" class="form-control" id="title" placeholder="title" required="" value="{{$data->title ?? old('title')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="subtitle" class="col-sm-2 control-label">Subtitle</label>

                  <div class="col-sm-10">
                    <input type="text" name="subtitle" class="form-control" id="subtitle" placeholder="subtitle" required="" value="{{$data->subtitle ?? old('subtitle')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="icon" class="col-sm-2 control-label">Icon</label>

                  <div class="col-sm-10">
                    <input type="text" name="icon" class="form-control" id="icon" placeholder="icon" required="" value="{{$data->icon ?? old('icon')}}">
                    <span>
                      <a href="https://www.bootstrapicons.com/index.htm?iconset=fontawesome&version=4.0.3" target="_blank">
                        You can get here
                      </a>
                    </span>
                    &nbsp
                    <span class="text-danger">fa fa-info-circle</span>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a type="button" href="{{$active['url']}}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Save</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
@endsection