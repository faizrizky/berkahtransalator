@extends('layouts.backend')
@section('title')
  {{ucfirst($active['title'])}}
@endsection
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ucfirst($active['title'])}}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <section class="col-lg-12">
          @if (Session::has('msg'))
          <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <center>
              <strong>{{ session('msg') }}</strong>
            </center>
          </div>
          @endif
        	<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Dashboard Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="{{$active['url']}}">
              @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="twitter" class="col-sm-2 control-label">Twitter</label>

                  <div class="col-sm-10">
                    <input type="text" name="twitter" class="form-control" id="twitter" placeholder="twitter" value="{{$data->twitter ?? old('twitter')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="instagram" class="col-sm-2 control-label">Instagram</label>

                  <div class="col-sm-10">
                    <input type="text" name="instagram" class="form-control" id="instagram" placeholder="instagram" value="{{$data->instagram ?? old('instagram')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="facebook" class="col-sm-2 control-label">Facebook</label>

                  <div class="col-sm-10">
                    <input type="text" name="facebook" class="form-control" id="facebook" placeholder="facebook" value="{{$data->facebook ?? old('facebook')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Email</label>

                  <div class="col-sm-10">
                    <input type="text" name="email" class="form-control" id="email" placeholder="email" value="{{$data->email ?? old('email')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="address" class="col-sm-2 control-label">Address</label>

                  <div class="col-sm-10">
                    <input type="text" name="address" class="form-control" id="address" placeholder="address" value="{{$data->address ?? old('address')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="phone" class="col-sm-2 control-label">Phone</label>

                  <div class="col-sm-10">
                    <input type="text" name="phone" class="form-control" id="phone" placeholder="phone" value="{{$data->phone ?? old('phone')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="title_1" class="col-sm-2 control-label">Title Slide 1</label>

                  <div class="col-sm-10">
                    <input type="text" name="title_1" class="form-control" id="title_1" placeholder="title slide 1" value="{{$data->title_1 ?? old('title_1')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="desc_1" class="col-sm-2 control-label">Description Slide 1</label>
                  <div class="col-sm-10">
                    <textarea name="desc_1" class="textarea-wysi"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$data->desc_1 ?? ''}}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="title_2" class="col-sm-2 control-label">Title Slide 2</label>

                  <div class="col-sm-10">
                    <input type="text" name="title_2" class="form-control" id="title_2" placeholder="title slide 2" value="{{$data->title_2 ?? old('title_2')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="desc_2a" class="col-sm-2 control-label">Subtitle Slide 2</label>

                  <div class="col-sm-10">
                    <input type="text" name="desc_2a" class="form-control" id="desc_2a" placeholder="subtitle slide 2" value="{{$data->desc_2a ?? old('desc_2a')}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="desc_2b" class="col-sm-2 control-label">Description Slide 2</label>
                  <div class="col-sm-10">
                    <textarea name="desc_2b" class="textarea-wysi"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$data->desc_2b ?? ''}}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="about_us" class="col-sm-2 control-label">About Us</label>
                  <div class="col-sm-10">
                    <textarea name="about_us" class="textarea-wysi"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$data->about_us ?? ''}}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a type="button" href="{{$active['url']}}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Save</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
@endsection
@push('appjs')
<script>
  $(function () {
    const calculation = (is_auto) => {
      if (is_auto == 1) {
        $('#hide-score').hide();
        $('input[name=score]').val(0);
      }else{
        $('#hide-score').show();
      }
    }
    $('#calculation').change(function(e) {
      calculation($(this).val());
    });
    @if (isset($data->is_auto))
      calculation({{$data->is_auto}});
    @endif
  })
</script>
@endpush