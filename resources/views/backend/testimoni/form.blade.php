@extends('layouts.backend')
@section('title')
  {{ucfirst($active['title'])}}
@endsection
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ucfirst($active['title'])}}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <section class="col-lg-12">
        	<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Product Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="{{$active['url']}}" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="id" value="{{$data->id ?? 0}}">
              <div class="box-body">
                <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Title</label>

                  <div class="col-sm-10">
                    <input type="text" name="title" class="form-control" id="title" placeholder="title" required="" value="{{$data->title ?? old('title')}}">
                  </div>
                </div>
               
                <div class="form-group">
                  <label for="image" class="col-sm-2 control-label">Image</label>
                  <div class="col-sm-10">
                    <input type="file" name="image" class="form-control" id="image" placeholder="Image" required="">
                    <input type="hidden" name="old_image" value="{{$data->image ?? ''}}">
                    @if(isset($data->image) && $data->image)
                      <a data-toggle="lightbox" href="{{url('')}}/{{$active['download_path']}}/{{$data->image ?? ''}}">
                        Show Image
                      </a>
                    @endif
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a type="button" href="{{$active['url']}}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Save</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    
@endsection