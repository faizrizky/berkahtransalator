<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8" />
    <title>Berkah Translator</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" type="image/png" href="{{ asset('wp-content/bower_components/carousel/images/logo.png') }}"/>
        
    <!-- CSS Files -->
        
    <link rel="stylesheet" href="{{ asset('wp-content/bower_components/carousel/css/reset.css') }}" />
    <link rel="stylesheet" href="{{ asset('wp-content/bower_components/carousel/css/animate.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('wp-content/bower_components/carousel/css/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('wp-content/bower_components/carousel/css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('wp-content/bower_components/carousel/css/font-awesome.css') }}" />
    <link rel="stylesheet" href="{{ asset('wp-content/bower_components/carousel/css/owl.carousel.css') }}" />
    <link rel="stylesheet" href="{{ asset('wp-content/bower_components/carousel/css/settings.css') }}" />
    <link rel="stylesheet" href="{{ asset('wp-content/bower_components/carousel/css/prettyPhoto.css') }}" />
    <link rel="stylesheet" href="{{ asset('wp-content/bower_components/carousel/css/responsive.css') }}" />
    <link rel="stylesheet" href="{{ asset('wp-content/bower_components/carousel/css/floating-wpp.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('wp-content/bower_components/carousel/css/player/YTPlayer.css') }}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />
    <style type="text/css">
        .toast {
            opacity: 1 !important;
        }
    </style>
    <!-- End CSS Files -->
    
</head>
    
    
<body data-spy="scroll" data-target=".nav-menu" data-offset="50">
    <div id="whatsapp" style="z-index: 1000"></div>
    <!-- Page Loader-->
    <div id="pageloader">   
        <div class="loader-item">
          <img src="{{ asset('wp-content/bower_components/carousel/images/loading.gif') }}" alt='Loading...' />
        </div>
    </div>
    <!-- End Page Loader-->

    <!-- Home Section -->
    <section id="home">
        <!-- Top Bar Section -->
        <section id="pagetop">
            <!-- Content -->
            <div class="content pagetop">
            
                <!-- Top Details-->
                <div class="col-xs-9 left">                     
                     <div class="top_details">
                        <span class="details-on-top hidden-in-mobile-a"><i class="fa fa-map-marker"></i>
                            {{$home->address ?? ''}}
                        </span>
                        <span class="details-on-top"><i class="fa fa-mobile"></i>
                            {{$home->phone ?? ''}}
                        </span>
                     </div>
                </div>
                <!-- End Top Details-->  
                
                <!-- Top Social Media-->             
                <div class="col-xs-3 right">
                    <div class="top-social-media">
                        @if(isset($home->twitter) && $home->twitter)
                            <a target="_blank" href="{{$home->twitter ?? '#'}}"><i class="fa fa-twitter"></i></a>
                        @endif
                        @if(isset($home->instagram) && $home->instagram)
                            <a target="_blank" href="{{$home->instagram ?? '#'}}"><i class="fa fa-instagram"></i></a>
                        @endif
                        @if(isset($home->facebook) && $home->facebook)
                            <a target="_blank" href="{{$home->facebook ?? '#'}}"><i class="fa fa-facebook"></i></a>
                        @endif
                    </div>
                </div>
                <!-- End Social Media--> 
                <div class="clear"></div>
            </div>
            <!-- End Content -->
        </section>
        <!-- End Top Bar Section -->
        
        <!-- Navigation Section -->
        <section id="navigation" class="shadow">
            <!-- Content -->
            <div class="content navigation">
                <!-- Logo -->
                <div class="logo">
                    <a class="scroll" href="#home"><img src="{{ asset('wp-content/bower_components/carousel/images/logo.png') }}" alt="Logo"/></a>
                </div>
                <!-- End Logo -->
                
                <!-- Nav Menu -->
                <div class="nav-menu">
                    <ul class="nav main-nav">
                        <li class="active"><a class="scroll" href="#home">Home</a></li>
                        <li><a class="scroll" href="#important_links">About</a></li>
                        <li><a class="scroll" href="#services">Services</a></li>
                        <li><a class="scroll" href="#portfolio">Testimoni</a></li>
                        <li><a class="scroll" href="#prices">Prices</a></li>                    
                        <li><a class="scroll" href="#contact">Order</a></li>              
                    </ul>
                </div>
                <!-- End Nav Menu -->
                
                <!-- Dropdown Menu For Mobile Devices-->
                <div class="dropdown mobile-drop">
                    <a data-toggle="dropdown" class="mobile-menu" href="#"><i class="fa fa-bars"></i></a>
                    <ul class="nav dropdown-menu fullwidth" role="menu" >
                        <li><a class="scroll" href="#home">Home</a></li>
                        <li><a class="scroll" href="#important_links">About</a></li>
                        <li><a class="scroll" href="#services">Services</a></li>
                        <li><a class="scroll" href="#prices">Prices</a></li>                    
                        <li><a class="scroll" href="#portfolio">Testimoni</a></li>               
                        <li><a class="scroll" href="#contact">Order</a></li>
                    </ul>
                </div>
                <!-- End Dropdown Menu For Mobile Devices-->
                <div class="clear"></div>
            </div>
            <!-- End Content -->
        </section>
        <!-- End Navigation Section -->
    
    
        <!-- Revolution Slider -->
        <section id="slider">
            <div class="tp-banner">
                <ul>
                    
                    <!-- Slide -->
                    <li class="revslide" data-transition="random" data-slotamount="7" data-masterspeed="800" >
                        <!-- MAIN IMAGE -->
                        <img src="{{ asset('wp-content/bower_components/carousel/images/rev-slider/slide2.jpg') }}"  alt="slidebg2"  data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">                 
                                            
                        <!-- Layer 1 -->
                        <div class="tp-caption lfb customout"
                            data-x="0" 
                            data-y="120" 
                            data-customin="x:0;y:50;z:0;rotationX:0;rotationY:0;rotationZ:0;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="1000"
                            data-start="1200"
                            data-easing="Power4.easeOut"
                            data-endspeed="500"
                            data-endeasing="Power4.easeIn"
                            >
                            <img src="{{ asset('wp-content/bower_components/carousel/images/rev-slider/write.png') }}" alt="" />
                        </div>
                        
                        <!-- Layer 3 -->
                        <div class="tp-caption  lft customout"
                            data-x="300"
                            data-y="50"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="1000"
                            data-start="2000"
                            data-easing="Power4.easeOut"
                            data-endspeed="500"
                            data-endeasing="Power4.easeIn"
                            data-captionhidden="on">
                            <h1 class="rev-text rev-title-a">{{$home->title_1 ?? ''}}</h1>
                        </div>
                        
                        <!-- Layer 4 -->
                        <div class="tp-caption  skewfromrightshort customout hidden-in-mobile-a"
                            data-x="300"
                            data-y="100"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="1000"
                            data-start="2300"
                            data-easing="Power4.easeOut"
                            data-endspeed="500"
                            data-endeasing="Power4.easeIn"
                            data-captionhidden="on">
                            <h2 class="rev-text-b rev-title-b">
                                {!!$home->title_1 ?? ''!!}
                            </h2>
                        </div>
                    </li>
                    
                    <!-- Slide -->
                    <li class="revslide" data-transition="random" data-slotamount="7" data-masterspeed="800" >
                        <!-- MAIN IMAGE -->
                        <img src="{{ asset('wp-content/bower_components/carousel/images/rev-slider/slide3.jpg') }}"  alt="slidebg3"  data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">
    
                        <!-- Layer 1 -->
                        <div class="tp-caption sfb customout"
                            data-x="200"
                            data-y="270"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="900"
                            data-start="900"
                            data-easing="Power4.easeOut"
                            data-endspeed="1300"
                            data-endeasing="Power4.easeIn"
                            data-captionhidden="on">
                            <img src="{{ asset('wp-content/bower_components/carousel/images/rev-slider/earth.png') }}" alt="earth" />
                        </div>
                        
                        <!-- Layer 2 -->
                        <div class="tp-caption sfl customout"
                            data-x="0"
                            data-y="50"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="900"
                            data-start="1200"
                            data-easing="Power4.easeOut"
                            data-endspeed="900"
                            data-endeasing="Power4.easeIn"
                            data-captionhidden="on">
                            <h6 class="rev-text rev-title-f">{{$home->title_2 ?? ''}}</h6>
                        </div>
                        
                        <!-- Layer 3 -->
                        <div class="tp-caption sfl customout hidden-in-mobile-c"
                            data-x="0"
                            data-y="130"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="900"
                            data-start="1500"
                            data-easing="Power4.easeOut"
                            data-endspeed="900"
                            data-endeasing="Power4.easeIn"
                            data-captionhidden="on">
                            <p class="rev-text p5">{{$home->desc_2a ?? ''}}</p>
                        </div>
                        
                        <!-- Layer 4 -->
                        <div class="tp-caption sfb customout hidden-in-mobile-a"
                            data-x="0"
                            data-y="160"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="900"
                            data-start="1800"
                            data-easing="Power4.easeOut"
                            data-endspeed="900"
                            data-endeasing="Power4.easeIn"
                            data-captionhidden="on">
                            <h2 class="rev-text-b rev-title-b">
                                {!!$home->desc_2b ?? ''!!}
                            </h2>
                        </div>
                        
                        <!-- Layer 6 -->
                        <div class="tp-caption sfb customout"
                            data-x="900"
                            data-y="30"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="900"
                            data-start="2700"
                            data-easing="Power4.easeOut"
                            data-endspeed="900"
                            data-endeasing="Power4.easeIn"
                            data-captionhidden="on">
                            <img src="{{ asset('wp-content/bower_components/carousel/images/rev-slider/girl.png') }}" alt="check"/>
                        </div>       
                    </li>
                </ul>
            </div>
        </section>
        <!-- End Revolution Slider -->
    </section>
    <!-- End Home-->
    
    <!-- Important Links -->
    <section id="important_links" class="main-content">
        <!-- Content -->
        <div class="content important_links animated" data-animation="fadeInUp" data-animation-delay="300">
            <!-- Important Links Carousel-->
            <div id="carousel-example-generic" class="carousel slide important_links_slide" data-ride="carousel">
                <!-- Slides Wrapper -->
                <div class="carousel-inner welcome-banner">
                    <!-- Slide -->
                    <div class="item active">
                        <div class="welcome-banner-text">
                            <h1 class="main_title_a">
                            <span>Welcome</span> To Our Site</h1>
                            <p>
                                {!!$home->about_us ?? ''!!}
                            </p>
                        </div>
                    </div>  
                    <!-- End Slide -->     
                </div>
                <!-- End Slides Wrapper -->
            </div>
            <!-- End Important Links Carousel-->
        </div>
        <!-- End Content -->
    </section>
    <!-- End Important Links -->
    
    <!-- About Section -->
    <section id="services" class="main-content">
        <!-- Content -->
        <div class="content">
        
            <!-- Main Title -->
            <h1 class="main_title_a animated" data-animation="fadeInUp" data-animation-delay="300">
                <span>What </span>we do
            </h1>
            
            <!-- What We Do -->
            <ul class="about-contents slide-boxes">
            
                <!-- Item -->
                @foreach($service as $key)
                <li class="about box animated" data-animation="rotateInDownLeft" data-animation-delay="100">
                    <a href="javascript:;" class="about-logo">
                        <i class="{{$key->icon}}"></i>
                    </a>
                    <h3>{{$key->title}}</h3>
                    <p>{{$key->subtitle}}</p>
                </li>
                @endforeach
                <!-- End Item -->

            </ul>
            <!-- End What We Do -->
            <div class="clear"></div>
            
        </div>
        <!-- End Content -->
    </section>
    <!-- End About Section-->   
                   
    <!-- Portfolio --> 
    <section id="portfolio" class="main-content">
            <div class="parallax-overlay">
          <!-- Content -->
          <div class="content">
                <!-- White Title -->
                <div class="section-title-white">
                    <h1>What our clients say</h1>
                    <div class="seperator"><span class="white_line"></span></div>
                </div>
                
                <!-- Client Say Carousel -->
                <div class="client-say-carousel">
                   <div id="carousel-client-say" class="carousel slide">
                        <!-- slides Wrapper -->
                        <div class="carousel-inner pretty-lightbox-a">
                            <!-- Slide -->
                            @foreach($testimoni as $key => $row)
                            <div class="item {{($key == 0) ? 'active':''}}">
                                @foreach($row as $testi)
                                <div class="work col-xs-4">
                                    <div class="work-inner">
                                        <div class="work-img work-img-crop">
                                            <img src="{{url('')}}/{{ $active['testimoni_path'] }}/{{$testi->image}}" alt=""/>
                                            <div class="mask">
                                                <a class="button zoom" href="{{url('')}}/{{ $active['testimoni_path'] }}/{{$testi->image}}"  data-rel="prettyPhoto[gallery]"><i class="fa fa-search"></i></a>
                                            </div>
                                            <div class="work-desc">
                                                <h4>{{$testi->title}}</h4>
                                            </div>                                      
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div> 
                            @endforeach
                            <!-- End Slide -->
                            
                          </div>
                          <!-- End slides Wrapper -->
            
                          <!-- Controls -->
                          <a class="left carousel-control s-controls" href="#carousel-client-say" data-slide="prev">
                             <i class="fa fa-angle-left"></i>
                          </a>
                          <a class="right carousel-control s-controls" href="#carousel-client-say" data-slide="next">
                             <i class="fa fa-angle-right"></i>
                          </a>
                          <!-- End Controls -->
                    </div>
                </div>
                <!-- End Client Say Carousel -->
                
          </div>
          <!-- End Content -->
          <span class="pattern1"></span>
       </div>
    </section>
    <!-- End Portfolio Section -->
                            
    <!-- Prices Section -->
    <section id="prices" class="main-content">
            <!-- Content -->
            <div class="content prices">
            
                <!-- Main Title -->
                <h1 class="main_title_a animated" data-animation="fadeInUp" data-animation-delay="300">
                    <span>Pricing</span> Tables
                </h1>
                
                {{-- first pricing table --}}
                @foreach($pricing as $idx => $row)
                <div class="plans-prices">
                    @php
                        $animated = 100;
                        $i = 0;
                    @endphp
                    @foreach($row as $key => $price)
                    <!-- Pricing Table -->
                    <div class="plan-price animated {{$i}} {{($i == 0) ? 'no-margin-left' : ''}} plan-bootom {{($price->highlight == 1) ? 'active' : ''}} {{($i == 3) ? 'no-margin-right' : ''}}" data-animation="rollIn" data-animation-delay="{{$animated}}">
                        <!-- Plan Header -->
                        <h1>{{$price->title}}</h1>
                        <!-- Plan Price -->
                        <div class="circle-price">
                            <h2>{{$price->price}}</h2>
                        </div>
                        <!-- Pricing List -->
                        <ul>
                            @php
                                $pricinglist = explode("\n", trim($price->content));
                            @endphp
                            @foreach($pricinglist as $list)
                                <li>{{$list}}</li>
                            @endforeach
                        </ul>
                        <a class="buy-button scroll" href="#contact" onclick="select_package({{$price->id}})">Buy Now</a>
                    </div>
                    @php
                        $i++;
                        $animated += 200;
                    @endphp
                    @endforeach
                    <div class="clear"></div>
                </div>
                @endforeach

                <!-- End Pricing plans -->
            </div>
            <!-- End Content -->
    </section>
    <!-- End Prices Section -->    
    
    <!-- Contact Section -->
    <section id="contact" class="main-content">
        <!-- Content -->
        <div class="content">
                        
                <!--Contact Info-->
                <div class="col-md-6 animated" data-animation="fadeInLeft" data-animation-delay="300">
                
                    <!-- Main Title -->
                    <h1 class="main_title_a align-left">
                        <span>Contact </span> Information
                    </h1>
                    
                    <!-- Address -->
                    <address>
                        <p>
                            <i class="fa fa-map-marker"></i><i class="fa fa-minus"></i>
                            {{$home->address ?? ''}}
                        </p>
                        <p><i class="fa fa-phone"></i><i class="fa fa-minus"></i>
                            {{$home->phone ?? ''}}
                        </p>
                        <p><i class="fa fa-add3 fa-envelope"></i><i class="fa fa-minus"></i>
                            {{$home->email ?? ''}}
                        </p>
                    </address>
                    <!-- End Address -->
                    
                    <!-- Social Media Icons -->
                    <div class="contact-social">
                        @if(isset($home->twitter) && $home->twitter)
                        <a target="_blank" href="{{$home->twitter}}">
                            <i class="fa fa-twitter"></i>
                        </a>
                        @endif
                        @if(isset($home->facebook) && $home->facebook)
                        <a target="_blank" href="{{$home->facebook}}">
                            <i class="fa fa-facebook"></i>
                        </a>
                        @endif
                        @if(isset($home->instagram) && $home->instagram)
                        <a target="_blank" href="{{$home->instagram}}">
                            <i class="fa fa-instagram"></i>
                        </a>
                        @endif
                    </div>
                    <!-- End Social Media Icons -->
                    
                </div>
                <!--End Contact Info-->

                <!--Contact Form-->
                <div class="col-md-6 animated" data-animation="fadeInRight" data-animation-delay="600">
                
                    <!-- Main Title -->
                    <h1 class="main_title_a align-left">
                        <span>Make </span> an Order
                    </h1>
                    
                    @if($errors->any())
                        <div class="alert alert-danger" style="display: block;">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {!! implode('', $errors->all('<div>:message</div>')) !!}
                        </div>
                    @endif
                    @if (Session::has('msg'))
                        <div class="alert alert-success" style="display: block;">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ session('msg') }}
                        </div>
                    @endif
                    <form action="{{$active['url']}}" method="post" id="contact-form" name="contact-form" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <input type="text" class="form-control " name="name" id="name" placeholder="Name" required>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <input type="email"  class="form-control" name="email" id="email" placeholder="Email" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <input type="text" class="form-control " name="phone" id="phone" placeholder="Phone" required>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <input type="subject"  class="form-control" name="subject" id="subject" placeholder="Subject" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <select class="form-control form-select" name="id_pricing" required id="package">
                                    <option value="">-- Select Package --</option>
                                    @foreach($pricing as $idx => $row)
                                        @foreach($row as $key => $price)
                                        <option value="{{$price->id}}">{{$price->title}}</option>
                                        @endforeach
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <input type="file" class="form-control" name="file" id="file" placeholder="File" required="required">
                                <span class="text-danger">format : doc, docx, pdf|max : 5Mb</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <textarea class="form-control" name="message" id="message" placeholder="Message" rows="9"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                {!! NoCaptcha::display(['data-size' => 'compact']) !!}
                            </div>
                        </div>

                        <p>
                            <button class="btn" id="submit" type="submit" name="submit" value="Submit">Submit</button>
                        </p>
                    </form>
                        
                </div>  
                <!--End Contact Form-->
                <div class="clear"></div>
        </div>
        <!-- End Content -->              
    </section>
    <!-- End Contact Section -->
    
    <!-- Footer Section -->
    <section id="footer" class="main-content">
        <div class="content footer">
            <div class="col-xs-12 align-center">
            
                <!-- Go To Top -->
                <div id="go-top" class="animated" data-animation="fadeInUp" data-animation-delay="0">
                    <a href="#home" class="scroll"><i class="fa fa-chevron-up"></i></a>
                </div><!-- End Go To Top -->
                
                <!-- Site Copyright -->
                <p class="footer-text copyright animated" data-animation="fadeInUp" data-animation-delay="700">
                    Copyright © 2021 - Berkah Translator. All Rights Reserved.
                    <br>
                    by
                    <a href="https://www.linkedin.com/in/faiz-andriansyah-749104126/" target="_blank"> 
                        <strong style="color: white">Faiz Rizky Andriansyah</strong>
                    </a>
                </p><!-- End Site Copyright -->
                
            </div>                      
            <div class="clear"></div>
        </div> 
        <!-- End Footer Content -->
    </section>
    <!-- End Footer Section -->
        
    <!-- JS Files -->
    
    <script type="text/javascript" src="{{ asset('wp-content/bower_components/carousel/js/jquery-1.10.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('wp-content/bower_components/carousel/js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('wp-content/bower_components/carousel/js/jquery.appear.js') }}"></script>
    <script type="text/javascript" src="{{ asset('wp-content/bower_components/carousel/js/jquery.countTo.js') }}"></script>
    <script type="text/javascript" src="{{ asset('wp-content/bower_components/carousel/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('wp-content/bower_components/carousel/js/jquery.prettyPhoto.js') }}"></script>
    <script type="text/javascript" src="{{ asset('wp-content/bower_components/carousel/js/modernizr-latest.js') }}"></script>
    <script type="text/javascript" src="{{ asset('wp-content/bower_components/carousel/js/SmoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('wp-content/bower_components/carousel/js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('wp-content/bower_components/carousel/js/jquery.easing.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('wp-content/bower_components/carousel/js/jquery.sticky.js') }}"></script>
    <script type="text/javascript" src="{{ asset('wp-content/bower_components/carousel/js/owl.carousel.js') }}"></script>
    <script type="text/javascript" src="{{ asset('wp-content/bower_components/carousel/js/floating-wpp.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('wp-content/bower_components/carousel/js/jquery.isotope.js') }}"></script>
    <script type="text/javascript" src="{{ asset('wp-content/bower_components/carousel/js/rev-slider/jquery.themepunch.plugins.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('wp-content/bower_components/carousel/js/rev-slider/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script type="text/javascript" src="{{ asset('wp-content/bower_components/carousel/js/scripts.js') }}"></script>

    <script type="text/javascript">
        $(function () {
            $('#whatsapp').floatingWhatsApp({
              phone: '{{$home->phone ?? ''}}',
              position: 'right'
            });
          });
        function select_package(id) {
            $('#package').val(id);
        }
    </script>
     {!! NoCaptcha::renderJs() !!}
    <!-- End JS Files -->

</body>

</html>