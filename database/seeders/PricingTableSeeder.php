<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PricingModel;
use Carbon\Carbon;

class PricingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
    	PricingModel::truncate();
    	$data = [
	        [
	        	'title' => 'pricing 1',
	        	'price' => '1000',
	        	'content' => null,
	        	'show' => 1,
	        	'highlight' => 0,
			    'created_at'        => $now,
			    'updated_at'        => $now,
	        ],
	        [
	        	'title' => 'pricing 2',
	        	'price' => '2000',
	        	'content' => null,
	        	'show' => 1,
	        	'highlight' => 1,
			    'created_at'        => $now,
			    'updated_at'        => $now,
	        ],
	        [
	        	'title' => 'pricing 3',
	        	'price' => '3000',
	        	'content' => null,
	        	'show' => 1,
	        	'highlight' => 0,
			    'created_at'        => $now,
			    'updated_at'        => $now,
	        ],
	        [
	        	'title' => 'pricing 4',
	        	'price' => '4000',
	        	'content' => null,
	        	'show' => 1,
	        	'highlight' => 0,
			    'created_at'        => $now,
			    'updated_at'        => $now,
	        ],
	        [
	        	'title' => 'pricing 5',
	        	'price' => '5000',
	        	'content' => null,
	        	'show' => 1,
	        	'highlight' => 0,
			    'created_at'        => $now,
			    'updated_at'        => $now,
	        ],
	        [
	        	'title' => 'pricing 6',
	        	'price' => '6000',
	        	'content' => null,
	        	'show' => 1,
	        	'highlight' => 0,
			    'created_at'        => $now,
			    'updated_at'        => $now,
	        ],
	        [
	        	'title' => 'pricing 7',
	        	'price' => '7000',
	        	'content' => null,
	        	'show' => 1,
	        	'highlight' => 0,
			    'created_at'        => $now,
			    'updated_at'        => $now,
	        ],
	        [
	        	'title' => 'pricing 8',
	        	'price' => '8000',
	        	'content' => null,
	        	'show' => 1,
	        	'highlight' => 0,
			    'created_at'        => $now,
			    'updated_at'        => $now,
	        ],
	        // etc
    	];
        PricingModel::insert($data);
    }
}
