<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
    	User::truncate();
    	$data = [
	        [
	        	'name' => 'Thomas',
	        	'email' => 'test@test.com',
	        	'password' => bcrypt('asdad'),
			    'created_at'        => $now,
			    'updated_at'        => $now,
	        ]
	        // etc
    	];
        User::insert($data);
    }
}
